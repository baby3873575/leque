<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Content controller
 */
class Content extends Admin_Controller
{
    protected $permissionCreate = 'Targets.Content.Create';
    protected $permissionDelete = 'Targets.Content.Delete';
    protected $permissionEdit   = 'Targets.Content.Edit';
    protected $permissionView   = 'Targets.Content.View';

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->auth->restrict($this->permissionView);
        $this->load->model('targets/targets_model');
        $this->lang->load('targets');
        
            $this->form_validation->set_error_delimiters("<span class='error'>", "</span>");
        
        Template::set_block('sub_nav', 'content/_sub_nav');

        Assets::add_module_js('targets', 'targets.js');
    }

    /**
     * Display a list of targets data.
     *
     * @return void
     */
    public function index()
    {
        // Deleting anything?
        if (isset($_POST['delete'])) {
            $this->auth->restrict($this->permissionDelete);
            $checked = $this->input->post('checked');
            if (is_array($checked) && count($checked)) {

                // If any of the deletions fail, set the result to false, so
                // failure message is set if any of the attempts fail, not just
                // the last attempt

                $result = true;
                foreach ($checked as $pid) {
                    $deleted = $this->targets_model->delete($pid);
                    if ($deleted == false) {
                        $result = false;
                    }
                }
                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('targets_delete_success'), 'success');
                } else {
                    Template::set_message(lang('targets_delete_failure') . $this->targets_model->error, 'error');
                }
            }
        }
        
        
        
        $records = $this->targets_model->find_all();

        Template::set('records', $records);
        
    Template::set('toolbar_title', lang('targets_manage'));

        Template::render();
    }
    
    /**
     * Create a targets object.
     *
     * @return void
     */
    public function create()
    {
        $this->auth->restrict($this->permissionCreate);
        
        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_targets()) {
                log_activity($this->auth->user_id(), lang('targets_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'targets');
                Template::set_message(lang('targets_create_success'), 'success');

                redirect(SITE_AREA . '/content/targets');
            }

            // Not validation error
            if ( ! empty($this->targets_model->error)) {
                Template::set_message(lang('targets_create_failure') . $this->targets_model->error, 'error');
            }
        }

        Template::set('toolbar_title', lang('targets_action_create'));

        Template::render();
    }
    /**
     * Allows editing of targets data.
     *
     * @return void
     */
    public function edit()
    {
        $id = $this->uri->segment(5);
        if (empty($id)) {
            Template::set_message(lang('targets_invalid_id'), 'error');

            redirect(SITE_AREA . '/content/targets');
        }
        
        if (isset($_POST['save'])) {
            $this->auth->restrict($this->permissionEdit);

            if ($this->save_targets('update', $id)) {
                log_activity($this->auth->user_id(), lang('targets_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'targets');
                Template::set_message(lang('targets_edit_success'), 'success');
                redirect(SITE_AREA . '/content/targets');
            }

            // Not validation error
            if ( ! empty($this->targets_model->error)) {
                Template::set_message(lang('targets_edit_failure') . $this->targets_model->error, 'error');
            }
        }
        
        elseif (isset($_POST['delete'])) {
            $this->auth->restrict($this->permissionDelete);

            if ($this->targets_model->delete($id)) {
                log_activity($this->auth->user_id(), lang('targets_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'targets');
                Template::set_message(lang('targets_delete_success'), 'success');

                redirect(SITE_AREA . '/content/targets');
            }

            Template::set_message(lang('targets_delete_failure') . $this->targets_model->error, 'error');
        }
        
        Template::set('targets', $this->targets_model->find($id));

        Template::set('toolbar_title', lang('targets_edit_heading'));
        Template::render();
    }

    //--------------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------------

    /**
     * Save the data.
     *
     * @param string $type Either 'insert' or 'update'.
     * @param int    $id   The ID of the record to update, ignored on inserts.
     *
     * @return boolean|integer An ID for successful inserts, true for successful
     * updates, else false.
     */
    private function save_targets($type = 'insert', $id = 0)
    {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // Validate the data
        $this->form_validation->set_rules($this->targets_model->get_validation_rules());
        if ($this->form_validation->run() === false) {
            return false;
        }

        // Make sure we only pass in the fields we want
        
        $data = $this->targets_model->prep_data($this->input->post());

        // Additional handling for default values should be added below,
        // or in the model's prep_data() method
        

        $return = false;
        if ($type == 'insert') {
            $id = $this->targets_model->insert($data);

            if (is_numeric($id)) {
                $return = $id;
            }
        } elseif ($type == 'update') {
            $return = $this->targets_model->update($id, $data);
        }

        return $return;
    }
}