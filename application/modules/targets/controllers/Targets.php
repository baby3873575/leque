<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Targets controller
 */
class Targets extends Front_Controller
{
    protected $permissionCreate = 'Targets.Targets.Create';
    protected $permissionDelete = 'Targets.Targets.Delete';
    protected $permissionEdit   = 'Targets.Targets.Edit';
    protected $permissionView   = 'Targets.Targets.View';

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('targets/targets_model');
        $this->lang->load('targets');
        
        

        Assets::add_module_js('targets', 'targets.js');
    }

    /**
     * Display a list of targets data.
     *
     * @return void
     */
    public function index()
    {
        
        
        
        
        $records = $this->targets_model->find_all();

        Template::set('records', $records);
        

        Template::render();
    }
    
}