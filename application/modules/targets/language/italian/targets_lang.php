<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['targets_manage']      = 'Gestisci targets';
$lang['targets_edit']        = 'Modifica';
$lang['targets_true']        = 'Vero';
$lang['targets_false']       = 'Falso';
$lang['targets_create']      = 'Crea';
$lang['targets_list']        = 'Elenca';
$lang['targets_new']       = 'Nuovo';
$lang['targets_edit_text']     = 'Modifica questo secondo le tue necessità';
$lang['targets_no_records']    = 'Non ci sono targets nel sistema.';
$lang['targets_create_new']    = 'Crea un nuovo targets.';
$lang['targets_create_success']  = 'targets creato con successo.';
$lang['targets_create_failure']  = 'C\'è stato un problema nella creazione di targets: ';
$lang['targets_create_new_button'] = 'Crea nuovo targets';
$lang['targets_invalid_id']    = 'ID targets non valido.';
$lang['targets_edit_success']    = 'targets creato con successo.';
$lang['targets_edit_failure']    = 'C\'è stato un errore nel salvataggio di targets: ';
$lang['targets_delete_success']  = 'record(s) creati con successo.';
$lang['targets_delete_failure']  = 'Non possiamo eliminare il record: ';
$lang['targets_delete_error']    = 'Non hai selezionato alcun record da eliminare.';
$lang['targets_actions']     = 'Azioni';
$lang['targets_cancel']      = 'Cancella';
$lang['targets_delete_record']   = 'Elimina questo targets';
$lang['targets_delete_confirm']  = 'Sei sicuro di voler eliminare questo targets?';
$lang['targets_edit_heading']    = 'Modifica targets';

// Create/Edit Buttons
$lang['targets_action_edit']   = 'Salva targets';
$lang['targets_action_create']   = 'Crea targets';

// Activities
$lang['targets_act_create_record'] = 'Creato il record con ID';
$lang['targets_act_edit_record'] = 'Aggiornato il record con ID';
$lang['targets_act_delete_record'] = 'Eliminato il record con ID';

// Listing Specifics
$lang['targets_records_empty']    = 'Nessun record trovato che corrisponda alla tua selezione.';
$lang['targets_errors_message']    = 'Per favore risolvi i seguenti errori:';

// Column Headings
$lang['targets_column_created']  = 'Creato';
$lang['targets_column_deleted']  = 'Eliminato';
$lang['targets_column_modified'] = 'Modificato';

// Module Details
$lang['targets_module_name'] = 'targets';
$lang['targets_module_description'] = 'Your module description';
$lang['targets_area_title'] = 'targets';

// Fields
$lang['targets_field_target_name'] = 'target_name';
$lang['targets_field_uuid'] = 'uuid';
$lang['targets_field_ava_seat_no'] = 'ava_seat_no';
$lang['targets_field_cur_seat_no'] = 'cur_seat_no';
$lang['targets_field_major'] = 'major';
$lang['targets_field_minior'] = 'minior';
