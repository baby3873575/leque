<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['targets_manage']      = 'Gerenciar targets';
$lang['targets_edit']        = 'Editar';
$lang['targets_true']        = 'Verdadeiro';
$lang['targets_false']       = 'Falso';
$lang['targets_create']      = 'Criar';
$lang['targets_list']        = 'Listar';
$lang['targets_new']       = 'Novo';
$lang['targets_edit_text']     = 'Edite isto conforme sua necessidade';
$lang['targets_no_records']    = 'Não há targets no sistema.';
$lang['targets_create_new']    = 'Criar novo(a) targets.';
$lang['targets_create_success']  = 'targets Criado(a) com sucesso.';
$lang['targets_create_failure']  = 'Ocorreu um problema criando o(a) targets: ';
$lang['targets_create_new_button'] = 'Criar novo(a) targets';
$lang['targets_invalid_id']    = 'ID de targets inválida.';
$lang['targets_edit_success']    = 'targets salvo(a) com sucesso.';
$lang['targets_edit_failure']    = 'Ocorreu um problema salvando o(a) targets: ';
$lang['targets_delete_success']  = 'Registro(s) excluído(s) com sucesso.';
$lang['targets_delete_failure']  = 'Não foi possível excluir o registro: ';
$lang['targets_delete_error']    = 'Voc6e não selecionou nenhum registro para excluir.';
$lang['targets_actions']     = 'Ações';
$lang['targets_cancel']      = 'Cancelar';
$lang['targets_delete_record']   = 'Excluir este(a) targets';
$lang['targets_delete_confirm']  = 'Você tem certeza que deseja excluir este(a) targets?';
$lang['targets_edit_heading']    = 'Editar targets';

// Create/Edit Buttons
$lang['targets_action_edit']   = 'Salvar targets';
$lang['targets_action_create']   = 'Criar targets';

// Activities
$lang['targets_act_create_record'] = 'Criado registro com ID';
$lang['targets_act_edit_record'] = 'Atualizado registro com ID';
$lang['targets_act_delete_record'] = 'Excluído registro com ID';

//Listing Specifics
$lang['targets_records_empty']    = 'Nenhum registro encontrado.';
$lang['targets_errors_message']    = 'Por favor corrija os erros a seguir:';

// Column Headings
$lang['targets_column_created']  = 'Criado';
$lang['targets_column_deleted']  = 'Excluído';
$lang['targets_column_modified'] = 'Atualizado';

// Module Details
$lang['targets_module_name'] = 'targets';
$lang['targets_module_description'] = 'Your module description';
$lang['targets_area_title'] = 'targets';

// Fields
$lang['targets_field_target_name'] = 'target_name';
$lang['targets_field_uuid'] = 'uuid';
$lang['targets_field_ava_seat_no'] = 'ava_seat_no';
$lang['targets_field_cur_seat_no'] = 'cur_seat_no';
$lang['targets_field_major'] = 'major';
$lang['targets_field_minior'] = 'minior';
