<?php defined('BASEPATH') || exit('No direct script access allowed');


$lang['targets_manage']      = 'Manage targets';
$lang['targets_edit']        = 'Edit';
$lang['targets_true']        = 'True';
$lang['targets_false']       = 'False';
$lang['targets_create']      = 'Create';
$lang['targets_list']        = 'List';
$lang['targets_new']       = 'New';
$lang['targets_edit_text']     = 'Edit this to suit your needs';
$lang['targets_no_records']    = 'There are no targets in the system.';
$lang['targets_create_new']    = 'Create a new targets.';
$lang['targets_create_success']  = 'targets successfully created.';
$lang['targets_create_failure']  = 'There was a problem creating the targets: ';
$lang['targets_create_new_button'] = 'Create New targets';
$lang['targets_invalid_id']    = 'Invalid targets ID.';
$lang['targets_edit_success']    = 'targets successfully saved.';
$lang['targets_edit_failure']    = 'There was a problem saving the targets: ';
$lang['targets_delete_success']  = 'record(s) successfully deleted.';
$lang['targets_delete_failure']  = 'We could not delete the record: ';
$lang['targets_delete_error']    = 'You have not selected any records to delete.';
$lang['targets_actions']     = 'Actions';
$lang['targets_cancel']      = 'Cancel';
$lang['targets_delete_record']   = 'Delete this targets';
$lang['targets_delete_confirm']  = 'Are you sure you want to delete this targets?';
$lang['targets_edit_heading']    = 'Edit targets';

// Create/Edit Buttons
$lang['targets_action_edit']   = 'Save targets';
$lang['targets_action_create']   = 'Create targets';

// Activities
$lang['targets_act_create_record'] = 'Created record with ID';
$lang['targets_act_edit_record'] = 'Updated record with ID';
$lang['targets_act_delete_record'] = 'Deleted record with ID';

//Listing Specifics
$lang['targets_records_empty']    = 'No records found that match your selection.';
$lang['targets_errors_message']    = 'Please fix the following errors:';

// Column Headings
$lang['targets_column_created']  = 'Created';
$lang['targets_column_deleted']  = 'Deleted';
$lang['targets_column_modified'] = 'Modified';
$lang['targets_column_deleted_by'] = 'Deleted By';
$lang['targets_column_created_by'] = 'Created By';
$lang['targets_column_modified_by'] = 'Modified By';

// Module Details
$lang['targets_module_name'] = 'targets';
$lang['targets_module_description'] = 'Your module description';
$lang['targets_area_title'] = 'targets';

// Fields
$lang['targets_field_target_name'] = 'target_name';
$lang['targets_field_uuid'] = 'uuid';
$lang['targets_field_ava_seat_no'] = 'ava_seat_no';
$lang['targets_field_cur_seat_no'] = 'cur_seat_no';
$lang['targets_field_major'] = 'major';
$lang['targets_field_minior'] = 'minior';