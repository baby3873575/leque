<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['targets_manage']      = 'Gestionar targets';
$lang['targets_edit']        = 'Editar';
$lang['targets_true']        = 'Verdadero';
$lang['targets_false']       = 'Falso';
$lang['targets_create']      = 'Crear';
$lang['targets_list']        = 'Listar';
$lang['targets_new']       = 'Nuevo';
$lang['targets_edit_text']     = 'Editar esto para satisfacer sus necesidades';
$lang['targets_no_records']    = 'Hay ninguna targets en la sistema.';
$lang['targets_create_new']    = 'Crear nuevo(a) targets.';
$lang['targets_create_success']  = 'targets creado(a) con éxito.';
$lang['targets_create_failure']  = 'Hubo un problema al crear el(la) targets: ';
$lang['targets_create_new_button'] = 'Crear nuevo(a) targets';
$lang['targets_invalid_id']    = 'ID de targets inválido(a).';
$lang['targets_edit_success']    = 'targets guardado correctamente.';
$lang['targets_edit_failure']    = 'Hubo un problema guardando el(la) targets: ';
$lang['targets_delete_success']  = 'Registro(s) eliminado con éxito.';
$lang['targets_delete_failure']  = 'No hemos podido eliminar el registro: ';
$lang['targets_delete_error']    = 'No ha seleccionado ning&#250;n registro que desea eliminar.';
$lang['targets_actions']     = 'Açciones';
$lang['targets_cancel']      = 'Cancelar';
$lang['targets_delete_record']   = 'Eliminar este(a) targets';
$lang['targets_delete_confirm']  = '¿Esta seguro de que desea eliminar este(a) targets?';
$lang['targets_edit_heading']    = 'Editar targets';

// Create/Edit Buttons
$lang['targets_action_edit']   = 'Guardar targets';
$lang['targets_action_create']   = 'Crear targets';

// Activities
$lang['targets_act_create_record'] = 'Creado registro con ID';
$lang['targets_act_edit_record'] = 'Actualizado registro con ID';
$lang['targets_act_delete_record'] = 'Eliminado registro con ID';

//Listing Specifics
$lang['targets_records_empty']    = 'No hay registros encontrados para su selección.';
$lang['targets_errors_message']    = 'Por favor corrija los siguientes errores:';

// Column Headings
$lang['targets_column_created']  = 'Creado';
$lang['targets_column_deleted']  = 'Elíminado';
$lang['targets_column_modified'] = 'Modificado';

// Module Details
$lang['targets_module_name'] = 'targets';
$lang['targets_module_description'] = 'Your module description';
$lang['targets_area_title'] = 'targets';

// Fields
$lang['targets_field_target_name'] = 'target_name';
$lang['targets_field_uuid'] = 'uuid';
$lang['targets_field_ava_seat_no'] = 'ava_seat_no';
$lang['targets_field_cur_seat_no'] = 'cur_seat_no';
$lang['targets_field_major'] = 'major';
$lang['targets_field_minior'] = 'minior';
