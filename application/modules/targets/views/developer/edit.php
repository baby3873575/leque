<?php

if (validation_errors()) :
?>
<div class='alert alert-block alert-error fade in'>
    <a class='close' data-dismiss='alert'>&times;</a>
    <h4 class='alert-heading'>
        <?php echo lang('targets_errors_message'); ?>
    </h4>
    <?php echo validation_errors(); ?>
</div>
<?php
endif;

$id = isset($targets->id) ? $targets->id : '';

?>
<div class='admin-box'>
    <h3>targets</h3>
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
        <fieldset>
            

            <div class="control-group<?php echo form_error('target_name') ? ' error' : ''; ?>">
                <?php echo form_label(lang('targets_field_target_name'), 'target_name', array('class' => 'control-label')); ?>
                <div class='controls'>
                    <input id='target_name' type='text' name='target_name' maxlength='255' value="<?php echo set_value('target_name', isset($targets->target_name) ? $targets->target_name : ''); ?>" />
                    <span class='help-inline'><?php echo form_error('target_name'); ?></span>
                </div>
            </div>

            <div class="control-group<?php echo form_error('uuid') ? ' error' : ''; ?>">
                <?php echo form_label(lang('targets_field_uuid'), 'uuid', array('class' => 'control-label')); ?>
                <div class='controls'>
                    <input id='uuid' type='text' name='uuid' maxlength='255' value="<?php echo set_value('uuid', isset($targets->uuid) ? $targets->uuid : ''); ?>" />
                    <span class='help-inline'><?php echo form_error('uuid'); ?></span>
                </div>
            </div>

            <div class="control-group<?php echo form_error('ava_seat_no') ? ' error' : ''; ?>">
                <?php echo form_label(lang('targets_field_ava_seat_no'), 'ava_seat_no', array('class' => 'control-label')); ?>
                <div class='controls'>
                    <input id='ava_seat_no' type='text' name='ava_seat_no' maxlength='255' value="<?php echo set_value('ava_seat_no', isset($targets->ava_seat_no) ? $targets->ava_seat_no : ''); ?>" />
                    <span class='help-inline'><?php echo form_error('ava_seat_no'); ?></span>
                </div>
            </div>

            <div class="control-group<?php echo form_error('cur_seat_no') ? ' error' : ''; ?>">
                <?php echo form_label(lang('targets_field_cur_seat_no'), 'cur_seat_no', array('class' => 'control-label')); ?>
                <div class='controls'>
                    <input id='cur_seat_no' type='text' name='cur_seat_no' maxlength='255' value="<?php echo set_value('cur_seat_no', isset($targets->cur_seat_no) ? $targets->cur_seat_no : ''); ?>" />
                    <span class='help-inline'><?php echo form_error('cur_seat_no'); ?></span>
                </div>
            </div>

            <div class="control-group<?php echo form_error('major') ? ' error' : ''; ?>">
                <?php echo form_label(lang('targets_field_major'), 'major', array('class' => 'control-label')); ?>
                <div class='controls'>
                    <input id='major' type='text' name='major'  value="<?php echo set_value('major', isset($targets->major) ? $targets->major : ''); ?>" />
                    <span class='help-inline'><?php echo form_error('major'); ?></span>
                </div>
            </div>

            <div class="control-group<?php echo form_error('minior') ? ' error' : ''; ?>">
                <?php echo form_label(lang('targets_field_minior'), 'minior', array('class' => 'control-label')); ?>
                <div class='controls'>
                    <input id='minior' type='text' name='minior'  value="<?php echo set_value('minior', isset($targets->minior) ? $targets->minior : ''); ?>" />
                    <span class='help-inline'><?php echo form_error('minior'); ?></span>
                </div>
            </div>
        </fieldset>
        <fieldset class='form-actions'>
            <input type='submit' name='save' class='btn btn-primary' value="<?php echo lang('targets_action_edit'); ?>" />
            <?php echo lang('bf_or'); ?>
            <?php echo anchor(SITE_AREA . '/developer/targets', lang('targets_cancel'), 'class="btn btn-warning"'); ?>
            
            <?php if ($this->auth->has_permission('Targets.Developer.Delete')) : ?>
                <?php echo lang('bf_or'); ?>
                <button type='submit' name='delete' formnovalidate class='btn btn-danger' id='delete-me' onclick="return confirm('<?php e(js_escape(lang('targets_delete_confirm'))); ?>');">
                    <span class='icon-trash icon-white'></span>&nbsp;<?php echo lang('targets_delete_record'); ?>
                </button>
            <?php endif; ?>
        </fieldset>
    <?php echo form_close(); ?>
</div>