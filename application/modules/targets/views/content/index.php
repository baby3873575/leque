<?php

$num_columns	= 6;
$can_delete	= $this->auth->has_permission('Targets.Content.Delete');
$can_edit		= $this->auth->has_permission('Targets.Content.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

if ($can_delete) {
    $num_columns++;
}
?>
<div class='admin-box'>
	<h3>
		<?php echo lang('targets_area_title'); ?>
	</h3>
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class='table table-striped'>
			<thead>
				<tr>
					<?php if ($can_delete && $has_records) : ?>
					<th class='column-check'><input class='check-all' type='checkbox' /></th>
					<?php endif;?>
					
					<th><?php echo lang('targets_field_target_name'); ?></th>
					<th><?php echo lang('targets_field_uuid'); ?></th>
					<th><?php echo lang('targets_field_ava_seat_no'); ?></th>
					<th><?php echo lang('targets_field_cur_seat_no'); ?></th>
					<th><?php echo lang('targets_field_major'); ?></th>
					<th><?php echo lang('targets_field_minior'); ?></th>
				</tr>
			</thead>
			<?php if ($has_records) : ?>
			<tfoot>
				<?php if ($can_delete) : ?>
				<tr>
					<td colspan='<?php echo $num_columns; ?>'>
						<?php echo lang('bf_with_selected'); ?>
						<input type='submit' name='delete' id='delete-me' class='btn btn-danger' value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('targets_delete_confirm'))); ?>')" />
					</td>
				</tr>
				<?php endif; ?>
			</tfoot>
			<?php endif; ?>
			<tbody>
				<?php
				if ($has_records) :
					foreach ($records as $record) :
				?>
				<tr>
					<?php if ($can_delete) : ?>
					<td class='column-check'><input type='checkbox' name='checked[]' value='<?php echo $record->id; ?>' /></td>
					<?php endif;?>
					
				<?php if ($can_edit) : ?>
					<td><?php echo anchor(SITE_AREA . '/content/targets/edit/' . $record->id, '<span class="icon-pencil"></span> ' .  $record->target_name); ?></td>
				<?php else : ?>
					<td><?php e($record->target_name); ?></td>
				<?php endif; ?>
					<td><?php e($record->uuid); ?></td>
					<td><?php e($record->ava_seat_no); ?></td>
					<td><?php e($record->cur_seat_no); ?></td>
					<td><?php e($record->major); ?></td>
					<td><?php e($record->minior); ?></td>
				</tr>
				<?php
					endforeach;
				else:
				?>
				<tr>
					<td colspan='<?php echo $num_columns; ?>'><?php echo lang('targets_records_empty'); ?></td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
	<?php
    echo form_close();
    
    ?>
</div>