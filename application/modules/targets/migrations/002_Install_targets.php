<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Install_targets extends Migration
{
	/**
	 * @var string The name of the database table
	 */
	private $table_name = 'targets';

	/**
	 * @var array The table's fields
	 */
	private $fields = array(
		'id' => array(
			'type'       => 'INT',
			'constraint' => 11,
			'auto_increment' => true,
		),
        'target_name' => array(
            'type'       => 'VARCHAR',
            'constraint' => 255,
            'null'       => true,
        ),
        'uuid' => array(
            'type'       => 'VARCHAR',
            'constraint' => 255,
            'null'       => true,
        ),
        'ava_seat_no' => array(
            'type'       => 'INT',
            'constraint' => 255,
            'null'       => true,
        ),
        'cur_seat_no' => array(
            'type'       => 'INT',
            'constraint' => 255,
            'null'       => true,
        ),
        'major' => array(
            'type'       => 'BIGINT',
            'null'       => true,
        ),
        'minior' => array(
            'type'       => 'BIGINT',
            'null'       => true,
        ),
	);

	/**
	 * Install this version
	 *
	 * @return void
	 */
	public function up()
	{
		$this->dbforge->add_field($this->fields);
		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table($this->table_name);
	}

	/**
	 * Uninstall this version
	 *
	 * @return void
	 */
	public function down()
	{
		$this->dbforge->drop_table($this->table_name);
	}
}