<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Install_targets_permissions extends Migration
{
	/**
	 * @var array Permissions to Migrate
	 */
	private $permissionValues = array(
		array(
			'name' => 'Targets.Content.View',
			'description' => 'View Targets Content',
			'status' => 'active',
		),
		array(
			'name' => 'Targets.Content.Create',
			'description' => 'Create Targets Content',
			'status' => 'active',
		),
		array(
			'name' => 'Targets.Content.Edit',
			'description' => 'Edit Targets Content',
			'status' => 'active',
		),
		array(
			'name' => 'Targets.Content.Delete',
			'description' => 'Delete Targets Content',
			'status' => 'active',
		),
		array(
			'name' => 'Targets.Reports.View',
			'description' => 'View Targets Reports',
			'status' => 'active',
		),
		array(
			'name' => 'Targets.Reports.Create',
			'description' => 'Create Targets Reports',
			'status' => 'active',
		),
		array(
			'name' => 'Targets.Reports.Edit',
			'description' => 'Edit Targets Reports',
			'status' => 'active',
		),
		array(
			'name' => 'Targets.Reports.Delete',
			'description' => 'Delete Targets Reports',
			'status' => 'active',
		),
		array(
			'name' => 'Targets.Settings.View',
			'description' => 'View Targets Settings',
			'status' => 'active',
		),
		array(
			'name' => 'Targets.Settings.Create',
			'description' => 'Create Targets Settings',
			'status' => 'active',
		),
		array(
			'name' => 'Targets.Settings.Edit',
			'description' => 'Edit Targets Settings',
			'status' => 'active',
		),
		array(
			'name' => 'Targets.Settings.Delete',
			'description' => 'Delete Targets Settings',
			'status' => 'active',
		),
		array(
			'name' => 'Targets.Developer.View',
			'description' => 'View Targets Developer',
			'status' => 'active',
		),
		array(
			'name' => 'Targets.Developer.Create',
			'description' => 'Create Targets Developer',
			'status' => 'active',
		),
		array(
			'name' => 'Targets.Developer.Edit',
			'description' => 'Edit Targets Developer',
			'status' => 'active',
		),
		array(
			'name' => 'Targets.Developer.Delete',
			'description' => 'Delete Targets Developer',
			'status' => 'active',
		),
    );

    /**
     * @var string The name of the permission key in the role_permissions table
     */
    private $permissionKey = 'permission_id';

    /**
     * @var string The name of the permission name field in the permissions table
     */
    private $permissionNameField = 'name';

	/**
	 * @var string The name of the role/permissions ref table
	 */
	private $rolePermissionsTable = 'role_permissions';

    /**
     * @var numeric The role id to which the permissions will be applied
     */
    private $roleId = '1';

    /**
     * @var string The name of the role key in the role_permissions table
     */
    private $roleKey = 'role_id';

	/**
	 * @var string The name of the permissions table
	 */
	private $tableName = 'permissions';

	//--------------------------------------------------------------------

	/**
	 * Install this version
	 *
	 * @return void
	 */
	public function up()
	{
		$rolePermissionsData = array();
		foreach ($this->permissionValues as $permissionValue) {
			$this->db->insert($this->tableName, $permissionValue);

			$rolePermissionsData[] = array(
                $this->roleKey       => $this->roleId,
                $this->permissionKey => $this->db->insert_id(),
			);
		}

		$this->db->insert_batch($this->rolePermissionsTable, $rolePermissionsData);
	}

	/**
	 * Uninstall this version
	 *
	 * @return void
	 */
	public function down()
	{
        $permissionNames = array();
		foreach ($this->permissionValues as $permissionValue) {
            $permissionNames[] = $permissionValue[$this->permissionNameField];
        }

        $query = $this->db->select($this->permissionKey)
                          ->where_in($this->permissionNameField, $permissionNames)
                          ->get($this->tableName);

        if ( ! $query->num_rows()) {
            return;
        }

        $permissionIds = array();
        foreach ($query->result() as $row) {
            $permissionIds[] = $row->{$this->permissionKey};
        }

        $this->db->where_in($this->permissionKey, $permissionIds)
                 ->delete($this->rolePermissionsTable);

        $this->db->where_in($this->permissionNameField, $permissionNames)
                 ->delete($this->tableName);
	}
}