<?php

$hiddenFields = array('id',);
?>
<h1 class='page-header'>
    <?php echo lang('que_area_title'); ?>
</h1>
<?php if (isset($records) && is_array($records) && count($records)) : ?>
<table class='table table-striped table-bordered'>
    <thead>
        <tr>
            
            <th>device_id</th>
            <th>target_id</th>
            <th>target_uuid</th>
            <th>t_major</th>
            <th>t_minior</th>
            <th>seat_num</th>
            <th>status</th>
            <th>final_choice</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($records as $record) :
        ?>
        <tr>
            <?php
            foreach($record as $field => $value) :
                if ( ! in_array($field, $hiddenFields)) :
            ?>
            <td>
                <?php
                if ($field == 'deleted') {
                    e(($value > 0) ? lang('que_true') : lang('que_false'));
                } else {
                    e($value);
                }
                ?>
            </td>
            <?php
                endif;
            endforeach;
            ?>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php

endif; ?>