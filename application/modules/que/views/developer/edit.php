<?php

if (validation_errors()) :
?>
<div class='alert alert-block alert-error fade in'>
    <a class='close' data-dismiss='alert'>&times;</a>
    <h4 class='alert-heading'>
        <?php echo lang('que_errors_message'); ?>
    </h4>
    <?php echo validation_errors(); ?>
</div>
<?php
endif;

$id = isset($que->id) ? $que->id : '';

?>
<div class='admin-box'>
    <h3>que</h3>
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
        <fieldset>
            

            <div class="control-group<?php echo form_error('device_id') ? ' error' : ''; ?>">
                <?php echo form_label(lang('que_field_device_id'), 'device_id', array('class' => 'control-label')); ?>
                <div class='controls'>
                    <input id='device_id' type='text' name='device_id' maxlength='255' value="<?php echo set_value('device_id', isset($que->device_id) ? $que->device_id : ''); ?>" />
                    <span class='help-inline'><?php echo form_error('device_id'); ?></span>
                </div>
            </div>

            <div class="control-group<?php echo form_error('target_id') ? ' error' : ''; ?>">
                <?php echo form_label(lang('que_field_target_id'), 'target_id', array('class' => 'control-label')); ?>
                <div class='controls'>
                    <input id='target_id' type='text' name='target_id' maxlength='20' value="<?php echo set_value('target_id', isset($que->target_id) ? $que->target_id : ''); ?>" />
                    <span class='help-inline'><?php echo form_error('target_id'); ?></span>
                </div>
            </div>

            <div class="control-group<?php echo form_error('target_uuid') ? ' error' : ''; ?>">
                <?php echo form_label(lang('que_field_target_uuid'), 'target_uuid', array('class' => 'control-label')); ?>
                <div class='controls'>
                    <input id='target_uuid' type='text' name='target_uuid' maxlength='255' value="<?php echo set_value('target_uuid', isset($que->target_uuid) ? $que->target_uuid : ''); ?>" />
                    <span class='help-inline'><?php echo form_error('target_uuid'); ?></span>
                </div>
            </div>

            <div class="control-group<?php echo form_error('t_major') ? ' error' : ''; ?>">
                <?php echo form_label(lang('que_field_t_major'), 't_major', array('class' => 'control-label')); ?>
                <div class='controls'>
                    <input id='t_major' type='text' name='t_major' maxlength='255' value="<?php echo set_value('t_major', isset($que->t_major) ? $que->t_major : ''); ?>" />
                    <span class='help-inline'><?php echo form_error('t_major'); ?></span>
                </div>
            </div>

            <div class="control-group<?php echo form_error('t_minior') ? ' error' : ''; ?>">
                <?php echo form_label(lang('que_field_t_minior'), 't_minior', array('class' => 'control-label')); ?>
                <div class='controls'>
                    <input id='t_minior' type='text' name='t_minior' maxlength='255' value="<?php echo set_value('t_minior', isset($que->t_minior) ? $que->t_minior : ''); ?>" />
                    <span class='help-inline'><?php echo form_error('t_minior'); ?></span>
                </div>
            </div>

            <div class="control-group<?php echo form_error('seat_num') ? ' error' : ''; ?>">
                <?php echo form_label(lang('que_field_seat_num'), 'seat_num', array('class' => 'control-label')); ?>
                <div class='controls'>
                    <input id='seat_num' type='text' name='seat_num' maxlength='100' value="<?php echo set_value('seat_num', isset($que->seat_num) ? $que->seat_num : ''); ?>" />
                    <span class='help-inline'><?php echo form_error('seat_num'); ?></span>
                </div>
            </div>

            <div class="control-group<?php echo form_error('status') ? ' error' : ''; ?>">
                <?php echo form_label(lang('que_field_status'), 'status', array('class' => 'control-label')); ?>
                <div class='controls'>
                    <input id='status' type='text' name='status' maxlength='10' value="<?php echo set_value('status', isset($que->status) ? $que->status : ''); ?>" />
                    <span class='help-inline'><?php echo form_error('status'); ?></span>
                </div>
            </div>

            <div class="control-group<?php echo form_error('final_choice') ? ' error' : ''; ?>">
                <?php echo form_label(lang('que_field_final_choice'), 'final_choice', array('class' => 'control-label')); ?>
                <div class='controls'>
                    <input id='final_choice' type='text' name='final_choice' maxlength='255' value="<?php echo set_value('final_choice', isset($que->final_choice) ? $que->final_choice : ''); ?>" />
                    <span class='help-inline'><?php echo form_error('final_choice'); ?></span>
                </div>
            </div>
        </fieldset>
        <fieldset class='form-actions'>
            <input type='submit' name='save' class='btn btn-primary' value="<?php echo lang('que_action_edit'); ?>" />
            <?php echo lang('bf_or'); ?>
            <?php echo anchor(SITE_AREA . '/developer/que', lang('que_cancel'), 'class="btn btn-warning"'); ?>
            
            <?php if ($this->auth->has_permission('Que.Developer.Delete')) : ?>
                <?php echo lang('bf_or'); ?>
                <button type='submit' name='delete' formnovalidate class='btn btn-danger' id='delete-me' onclick="return confirm('<?php e(js_escape(lang('que_delete_confirm'))); ?>');">
                    <span class='icon-trash icon-white'></span>&nbsp;<?php echo lang('que_delete_record'); ?>
                </button>
            <?php endif; ?>
        </fieldset>
    <?php echo form_close(); ?>
</div>