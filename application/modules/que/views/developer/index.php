<?php

$num_columns	= 8;
$can_delete	= $this->auth->has_permission('Que.Developer.Delete');
$can_edit		= $this->auth->has_permission('Que.Developer.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

if ($can_delete) {
    $num_columns++;
}
?>
<div class='admin-box'>
	<h3>
		<?php echo lang('que_area_title'); ?>
	</h3>
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class='table table-striped'>
			<thead>
				<tr>
					<?php if ($can_delete && $has_records) : ?>
					<th class='column-check'><input class='check-all' type='checkbox' /></th>
					<?php endif;?>
					
					<th><?php echo lang('que_field_device_id'); ?></th>
					<th><?php echo lang('que_field_target_id'); ?></th>
					<th><?php echo lang('que_field_target_uuid'); ?></th>
					<th><?php echo lang('que_field_t_major'); ?></th>
					<th><?php echo lang('que_field_t_minior'); ?></th>
					<th><?php echo lang('que_field_seat_num'); ?></th>
					<th><?php echo lang('que_field_status'); ?></th>
					<th><?php echo lang('que_field_final_choice'); ?></th>
				</tr>
			</thead>
			<?php if ($has_records) : ?>
			<tfoot>
				<?php if ($can_delete) : ?>
				<tr>
					<td colspan='<?php echo $num_columns; ?>'>
						<?php echo lang('bf_with_selected'); ?>
						<input type='submit' name='delete' id='delete-me' class='btn btn-danger' value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('que_delete_confirm'))); ?>')" />
					</td>
				</tr>
				<?php endif; ?>
			</tfoot>
			<?php endif; ?>
			<tbody>
				<?php
				if ($has_records) :
					foreach ($records as $record) :
				?>
				<tr>
					<?php if ($can_delete) : ?>
					<td class='column-check'><input type='checkbox' name='checked[]' value='<?php echo $record->id; ?>' /></td>
					<?php endif;?>
					
				<?php if ($can_edit) : ?>
					<td><?php echo anchor(SITE_AREA . '/developer/que/edit/' . $record->id, '<span class="icon-pencil"></span> ' .  $record->device_id); ?></td>
				<?php else : ?>
					<td><?php e($record->device_id); ?></td>
				<?php endif; ?>
					<td><?php e($record->target_id); ?></td>
					<td><?php e($record->target_uuid); ?></td>
					<td><?php e($record->t_major); ?></td>
					<td><?php e($record->t_minior); ?></td>
					<td><?php e($record->seat_num); ?></td>
					<td><?php e($record->status); ?></td>
					<td><?php e($record->final_choice); ?></td>
				</tr>
				<?php
					endforeach;
				else:
				?>
				<tr>
					<td colspan='<?php echo $num_columns; ?>'><?php echo lang('que_records_empty'); ?></td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
	<?php
    echo form_close();
    
    ?>
</div>