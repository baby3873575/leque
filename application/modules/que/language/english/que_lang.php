<?php defined('BASEPATH') || exit('No direct script access allowed');


$lang['que_manage']      = 'Manage que';
$lang['que_edit']        = 'Edit';
$lang['que_true']        = 'True';
$lang['que_false']       = 'False';
$lang['que_create']      = 'Create';
$lang['que_list']        = 'List';
$lang['que_new']       = 'New';
$lang['que_edit_text']     = 'Edit this to suit your needs';
$lang['que_no_records']    = 'There are no que in the system.';
$lang['que_create_new']    = 'Create a new que.';
$lang['que_create_success']  = 'que successfully created.';
$lang['que_create_failure']  = 'There was a problem creating the que: ';
$lang['que_create_new_button'] = 'Create New que';
$lang['que_invalid_id']    = 'Invalid que ID.';
$lang['que_edit_success']    = 'que successfully saved.';
$lang['que_edit_failure']    = 'There was a problem saving the que: ';
$lang['que_delete_success']  = 'record(s) successfully deleted.';
$lang['que_delete_failure']  = 'We could not delete the record: ';
$lang['que_delete_error']    = 'You have not selected any records to delete.';
$lang['que_actions']     = 'Actions';
$lang['que_cancel']      = 'Cancel';
$lang['que_delete_record']   = 'Delete this que';
$lang['que_delete_confirm']  = 'Are you sure you want to delete this que?';
$lang['que_edit_heading']    = 'Edit que';

// Create/Edit Buttons
$lang['que_action_edit']   = 'Save que';
$lang['que_action_create']   = 'Create que';

// Activities
$lang['que_act_create_record'] = 'Created record with ID';
$lang['que_act_edit_record'] = 'Updated record with ID';
$lang['que_act_delete_record'] = 'Deleted record with ID';

//Listing Specifics
$lang['que_records_empty']    = 'No records found that match your selection.';
$lang['que_errors_message']    = 'Please fix the following errors:';

// Column Headings
$lang['que_column_created']  = 'Created';
$lang['que_column_deleted']  = 'Deleted';
$lang['que_column_modified'] = 'Modified';
$lang['que_column_deleted_by'] = 'Deleted By';
$lang['que_column_created_by'] = 'Created By';
$lang['que_column_modified_by'] = 'Modified By';

// Module Details
$lang['que_module_name'] = 'que';
$lang['que_module_description'] = 'Your module description';
$lang['que_area_title'] = 'que';

// Fields
$lang['que_field_device_id'] = 'device_id';
$lang['que_field_target_id'] = 'target_id';
$lang['que_field_target_uuid'] = 'target_uuid';
$lang['que_field_t_major'] = 't_major';
$lang['que_field_t_minior'] = 't_minior';
$lang['que_field_seat_num'] = 'seat_num';
$lang['que_field_status'] = 'status';
$lang['que_field_final_choice'] = 'final_choice';