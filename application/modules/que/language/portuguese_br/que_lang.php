<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['que_manage']      = 'Gerenciar que';
$lang['que_edit']        = 'Editar';
$lang['que_true']        = 'Verdadeiro';
$lang['que_false']       = 'Falso';
$lang['que_create']      = 'Criar';
$lang['que_list']        = 'Listar';
$lang['que_new']       = 'Novo';
$lang['que_edit_text']     = 'Edite isto conforme sua necessidade';
$lang['que_no_records']    = 'Não há que no sistema.';
$lang['que_create_new']    = 'Criar novo(a) que.';
$lang['que_create_success']  = 'que Criado(a) com sucesso.';
$lang['que_create_failure']  = 'Ocorreu um problema criando o(a) que: ';
$lang['que_create_new_button'] = 'Criar novo(a) que';
$lang['que_invalid_id']    = 'ID de que inválida.';
$lang['que_edit_success']    = 'que salvo(a) com sucesso.';
$lang['que_edit_failure']    = 'Ocorreu um problema salvando o(a) que: ';
$lang['que_delete_success']  = 'Registro(s) excluído(s) com sucesso.';
$lang['que_delete_failure']  = 'Não foi possível excluir o registro: ';
$lang['que_delete_error']    = 'Voc6e não selecionou nenhum registro para excluir.';
$lang['que_actions']     = 'Ações';
$lang['que_cancel']      = 'Cancelar';
$lang['que_delete_record']   = 'Excluir este(a) que';
$lang['que_delete_confirm']  = 'Você tem certeza que deseja excluir este(a) que?';
$lang['que_edit_heading']    = 'Editar que';

// Create/Edit Buttons
$lang['que_action_edit']   = 'Salvar que';
$lang['que_action_create']   = 'Criar que';

// Activities
$lang['que_act_create_record'] = 'Criado registro com ID';
$lang['que_act_edit_record'] = 'Atualizado registro com ID';
$lang['que_act_delete_record'] = 'Excluído registro com ID';

//Listing Specifics
$lang['que_records_empty']    = 'Nenhum registro encontrado.';
$lang['que_errors_message']    = 'Por favor corrija os erros a seguir:';

// Column Headings
$lang['que_column_created']  = 'Criado';
$lang['que_column_deleted']  = 'Excluído';
$lang['que_column_modified'] = 'Atualizado';

// Module Details
$lang['que_module_name'] = 'que';
$lang['que_module_description'] = 'Your module description';
$lang['que_area_title'] = 'que';

// Fields
$lang['que_field_device_id'] = 'device_id';
$lang['que_field_target_id'] = 'target_id';
$lang['que_field_target_uuid'] = 'target_uuid';
$lang['que_field_t_major'] = 't_major';
$lang['que_field_t_minior'] = 't_minior';
$lang['que_field_seat_num'] = 'seat_num';
$lang['que_field_status'] = 'status';
$lang['que_field_final_choice'] = 'final_choice';
