<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['que_manage']      = 'Gestisci que';
$lang['que_edit']        = 'Modifica';
$lang['que_true']        = 'Vero';
$lang['que_false']       = 'Falso';
$lang['que_create']      = 'Crea';
$lang['que_list']        = 'Elenca';
$lang['que_new']       = 'Nuovo';
$lang['que_edit_text']     = 'Modifica questo secondo le tue necessità';
$lang['que_no_records']    = 'Non ci sono que nel sistema.';
$lang['que_create_new']    = 'Crea un nuovo que.';
$lang['que_create_success']  = 'que creato con successo.';
$lang['que_create_failure']  = 'C\'è stato un problema nella creazione di que: ';
$lang['que_create_new_button'] = 'Crea nuovo que';
$lang['que_invalid_id']    = 'ID que non valido.';
$lang['que_edit_success']    = 'que creato con successo.';
$lang['que_edit_failure']    = 'C\'è stato un errore nel salvataggio di que: ';
$lang['que_delete_success']  = 'record(s) creati con successo.';
$lang['que_delete_failure']  = 'Non possiamo eliminare il record: ';
$lang['que_delete_error']    = 'Non hai selezionato alcun record da eliminare.';
$lang['que_actions']     = 'Azioni';
$lang['que_cancel']      = 'Cancella';
$lang['que_delete_record']   = 'Elimina questo que';
$lang['que_delete_confirm']  = 'Sei sicuro di voler eliminare questo que?';
$lang['que_edit_heading']    = 'Modifica que';

// Create/Edit Buttons
$lang['que_action_edit']   = 'Salva que';
$lang['que_action_create']   = 'Crea que';

// Activities
$lang['que_act_create_record'] = 'Creato il record con ID';
$lang['que_act_edit_record'] = 'Aggiornato il record con ID';
$lang['que_act_delete_record'] = 'Eliminato il record con ID';

// Listing Specifics
$lang['que_records_empty']    = 'Nessun record trovato che corrisponda alla tua selezione.';
$lang['que_errors_message']    = 'Per favore risolvi i seguenti errori:';

// Column Headings
$lang['que_column_created']  = 'Creato';
$lang['que_column_deleted']  = 'Eliminato';
$lang['que_column_modified'] = 'Modificato';

// Module Details
$lang['que_module_name'] = 'que';
$lang['que_module_description'] = 'Your module description';
$lang['que_area_title'] = 'que';

// Fields
$lang['que_field_device_id'] = 'device_id';
$lang['que_field_target_id'] = 'target_id';
$lang['que_field_target_uuid'] = 'target_uuid';
$lang['que_field_t_major'] = 't_major';
$lang['que_field_t_minior'] = 't_minior';
$lang['que_field_seat_num'] = 'seat_num';
$lang['que_field_status'] = 'status';
$lang['que_field_final_choice'] = 'final_choice';
