<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['que_manage']      = 'Gestionar que';
$lang['que_edit']        = 'Editar';
$lang['que_true']        = 'Verdadero';
$lang['que_false']       = 'Falso';
$lang['que_create']      = 'Crear';
$lang['que_list']        = 'Listar';
$lang['que_new']       = 'Nuevo';
$lang['que_edit_text']     = 'Editar esto para satisfacer sus necesidades';
$lang['que_no_records']    = 'Hay ninguna que en la sistema.';
$lang['que_create_new']    = 'Crear nuevo(a) que.';
$lang['que_create_success']  = 'que creado(a) con éxito.';
$lang['que_create_failure']  = 'Hubo un problema al crear el(la) que: ';
$lang['que_create_new_button'] = 'Crear nuevo(a) que';
$lang['que_invalid_id']    = 'ID de que inválido(a).';
$lang['que_edit_success']    = 'que guardado correctamente.';
$lang['que_edit_failure']    = 'Hubo un problema guardando el(la) que: ';
$lang['que_delete_success']  = 'Registro(s) eliminado con éxito.';
$lang['que_delete_failure']  = 'No hemos podido eliminar el registro: ';
$lang['que_delete_error']    = 'No ha seleccionado ning&#250;n registro que desea eliminar.';
$lang['que_actions']     = 'Açciones';
$lang['que_cancel']      = 'Cancelar';
$lang['que_delete_record']   = 'Eliminar este(a) que';
$lang['que_delete_confirm']  = '¿Esta seguro de que desea eliminar este(a) que?';
$lang['que_edit_heading']    = 'Editar que';

// Create/Edit Buttons
$lang['que_action_edit']   = 'Guardar que';
$lang['que_action_create']   = 'Crear que';

// Activities
$lang['que_act_create_record'] = 'Creado registro con ID';
$lang['que_act_edit_record'] = 'Actualizado registro con ID';
$lang['que_act_delete_record'] = 'Eliminado registro con ID';

//Listing Specifics
$lang['que_records_empty']    = 'No hay registros encontrados para su selección.';
$lang['que_errors_message']    = 'Por favor corrija los siguientes errores:';

// Column Headings
$lang['que_column_created']  = 'Creado';
$lang['que_column_deleted']  = 'Elíminado';
$lang['que_column_modified'] = 'Modificado';

// Module Details
$lang['que_module_name'] = 'que';
$lang['que_module_description'] = 'Your module description';
$lang['que_area_title'] = 'que';

// Fields
$lang['que_field_device_id'] = 'device_id';
$lang['que_field_target_id'] = 'target_id';
$lang['que_field_target_uuid'] = 'target_uuid';
$lang['que_field_t_major'] = 't_major';
$lang['que_field_t_minior'] = 't_minior';
$lang['que_field_seat_num'] = 'seat_num';
$lang['que_field_status'] = 'status';
$lang['que_field_final_choice'] = 'final_choice';
