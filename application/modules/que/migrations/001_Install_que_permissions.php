<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Install_que_permissions extends Migration
{
	/**
	 * @var array Permissions to Migrate
	 */
	private $permissionValues = array(
		array(
			'name' => 'Que.Content.View',
			'description' => 'View Que Content',
			'status' => 'active',
		),
		array(
			'name' => 'Que.Content.Create',
			'description' => 'Create Que Content',
			'status' => 'active',
		),
		array(
			'name' => 'Que.Content.Edit',
			'description' => 'Edit Que Content',
			'status' => 'active',
		),
		array(
			'name' => 'Que.Content.Delete',
			'description' => 'Delete Que Content',
			'status' => 'active',
		),
		array(
			'name' => 'Que.Reports.View',
			'description' => 'View Que Reports',
			'status' => 'active',
		),
		array(
			'name' => 'Que.Reports.Create',
			'description' => 'Create Que Reports',
			'status' => 'active',
		),
		array(
			'name' => 'Que.Reports.Edit',
			'description' => 'Edit Que Reports',
			'status' => 'active',
		),
		array(
			'name' => 'Que.Reports.Delete',
			'description' => 'Delete Que Reports',
			'status' => 'active',
		),
		array(
			'name' => 'Que.Settings.View',
			'description' => 'View Que Settings',
			'status' => 'active',
		),
		array(
			'name' => 'Que.Settings.Create',
			'description' => 'Create Que Settings',
			'status' => 'active',
		),
		array(
			'name' => 'Que.Settings.Edit',
			'description' => 'Edit Que Settings',
			'status' => 'active',
		),
		array(
			'name' => 'Que.Settings.Delete',
			'description' => 'Delete Que Settings',
			'status' => 'active',
		),
		array(
			'name' => 'Que.Developer.View',
			'description' => 'View Que Developer',
			'status' => 'active',
		),
		array(
			'name' => 'Que.Developer.Create',
			'description' => 'Create Que Developer',
			'status' => 'active',
		),
		array(
			'name' => 'Que.Developer.Edit',
			'description' => 'Edit Que Developer',
			'status' => 'active',
		),
		array(
			'name' => 'Que.Developer.Delete',
			'description' => 'Delete Que Developer',
			'status' => 'active',
		),
    );

    /**
     * @var string The name of the permission key in the role_permissions table
     */
    private $permissionKey = 'permission_id';

    /**
     * @var string The name of the permission name field in the permissions table
     */
    private $permissionNameField = 'name';

	/**
	 * @var string The name of the role/permissions ref table
	 */
	private $rolePermissionsTable = 'role_permissions';

    /**
     * @var numeric The role id to which the permissions will be applied
     */
    private $roleId = '1';

    /**
     * @var string The name of the role key in the role_permissions table
     */
    private $roleKey = 'role_id';

	/**
	 * @var string The name of the permissions table
	 */
	private $tableName = 'permissions';

	//--------------------------------------------------------------------

	/**
	 * Install this version
	 *
	 * @return void
	 */
	public function up()
	{
		$rolePermissionsData = array();
		foreach ($this->permissionValues as $permissionValue) {
			$this->db->insert($this->tableName, $permissionValue);

			$rolePermissionsData[] = array(
                $this->roleKey       => $this->roleId,
                $this->permissionKey => $this->db->insert_id(),
			);
		}

		$this->db->insert_batch($this->rolePermissionsTable, $rolePermissionsData);
	}

	/**
	 * Uninstall this version
	 *
	 * @return void
	 */
	public function down()
	{
        $permissionNames = array();
		foreach ($this->permissionValues as $permissionValue) {
            $permissionNames[] = $permissionValue[$this->permissionNameField];
        }

        $query = $this->db->select($this->permissionKey)
                          ->where_in($this->permissionNameField, $permissionNames)
                          ->get($this->tableName);

        if ( ! $query->num_rows()) {
            return;
        }

        $permissionIds = array();
        foreach ($query->result() as $row) {
            $permissionIds[] = $row->{$this->permissionKey};
        }

        $this->db->where_in($this->permissionKey, $permissionIds)
                 ->delete($this->rolePermissionsTable);

        $this->db->where_in($this->permissionNameField, $permissionNames)
                 ->delete($this->tableName);
	}
}