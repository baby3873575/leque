<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Que controller
 */
class Que extends Front_Controller
{
    protected $permissionCreate = 'Que.Que.Create';
    protected $permissionDelete = 'Que.Que.Delete';
    protected $permissionEdit   = 'Que.Que.Edit';
    protected $permissionView   = 'Que.Que.View';

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('que/que_model');
        $this->lang->load('que');
        
        

        Assets::add_module_js('que', 'que.js');
    }

    /**
     * Display a list of que data.
     *
     * @return void
     */
    public function index()
    {
        
        
        
        
        $records = $this->que_model->find_all();

        Template::set('records', $records);
        

        Template::render();
    }
    
}